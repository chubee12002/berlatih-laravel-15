<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'judul' => ['required', 'unique:pertanyaan'],
            'isi' => ['required'],
        ]);
        $tglBuat=date('Y-m-d');
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "tanggal_dibuat" => $tglBuat,
            "tanggal_diperbaharui" => $tglBuat,
        ]);
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Disimpan');
    }

    public function index()
    {
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', ['pertanyaan' => $pertanyaan]);
    }

    public function show($id)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.show', ['pertanyaan' => $pertanyaan]);
    }

    public function edit($id)
    {
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', ['pertanyaan' => $pertanyaan]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => ['required', 'unique:pertanyaan'],
            'isi' => ['required'],
        ]);

        $query = DB::table('pertanyaan')
            ->where('id', $id)
            ->update([
                'judul' => $request["judul"],
                'isi' => $request["isi"]
            ]);
        return redirect('/pertanyaan')->with('success', 'Berhasil update pertanyaan');
    }

    public function destroy($id)
    {
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan');
    }

}
